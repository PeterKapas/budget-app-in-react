import { Button, Stack } from 'react-bootstrap'
import Container from 'react-bootstrap/Container'
import AddBudgetModal from './components/AddBudgetModal'
import BudgetCard from './components/BudgetCard'
import { useState } from 'react'
import { useBudgets } from './contexts/BudgetsContexts'

export default function App() {
	const [showAddBudgetModal, setShowAddBudgetModal] = useState(false)
	const { budgets, getBudgetExpenses } = useBudgets()
	return (
		<>
			<Container className='my-4'>
				<Stack className='mb-4' direction='horizontal' gap='2'>
					<h1 className='me-auto'>Budgets</h1>
					<Button variant='primary' onClick={() => setShowAddBudgetModal(true)}>
						Add Budget
					</Button>
					<Button variant='outline-primary'>Add expense</Button>
				</Stack>
				<div
					style={{
						display: 'grid',
						gridTemplateColumns: 'repeat(auto-fill,minmax(300px, 1fr))',
						gap: '1rem',
						alignItems: 'flex-start',
					}}
				>
					{budgets.map((budget) => {
						const amount = getBudgetExpenses(budget.id).reduce(
							(total, expense) => total + expense.amount,
							0
						)
						return (
							<BudgetCard
								key={budget.id}
								name={budget.name}
								amount={amount}
								max={budget.max}
							/>
						)
					})}
				</div>
			</Container>
			<AddBudgetModal
				show={showAddBudgetModal}
				handleClose={() => setShowAddBudgetModal(false)}
			/>
		</>
	)
}
